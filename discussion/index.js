// Aggregation

// Create documents for fruits database
db.fruits.insertMany([
  {
    name: "Apple",
    color: "Red",
    stock: 20,
    price: 40,
    supplier_id: 1,
    onSale: true,
    origin: ["Philippines", "US"],
  },
  {
    name: "Banana",
    color: "Yellow",
    stock: 15,
    price: 20,
    supplier_id: 2,
    onSale: true,
    origin: ["Philippines", "Ecuador"],
  },
  {
    name: "Kiwi",
    color: "Green",
    stock: 25,
    price: 50,
    supplier_id: 1,
    onSale: true,
    origin: ["US", "China"],
  },
  {
    name: "Mango",
    color: "Yellow",
    stock: 10,
    price: 120,
    supplier_id: 2,
    onSale: false,
    origin: ["Philippines", "India"],
  },
]);

// MongoDB Aggregation
/* 
    - Used to generate manipulated data and perform operations to create filtered results

    Using Aggregate Methods
    - $match - used to pass documents that meet specified condiitions
    - Syntax
        { $match: { field: value } }

    - $group - used to group documents by a specified field
    - Syntax
        db.collection.aggregate([
            { $match: { field: value } }
            { $group: { _id: $field, newField: { $sum: $field } } }
        } }
        ])
*/

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
]);

// Field projection with aggregation
/* 
    - $project - used when aggregating data to include/exclude fields from the returned results
    - Syntax
        - { $project: { field: 1/0 } }
*/

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
  { $project: { _id: 0 } },
]);

// $sort
/*
    - $sort - used to sort documents in ascending/descending order
    - Syntax
        - { $sort: { field: 1/-1 } }
*/

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
  { $sort: { total: -1 } },
]);

// Aggregating based on array fields
/* 
    - $unwind - used to deconstruct array fields
    - Syntax
        - { $unwind: "$field" }
*/

db.fruits.aggregate([{ $unwind: "$origin" }]);

// Displays fruit documents by their origin and the kinds of fruits that are supplied
db.fruits.aggregate([
  { $unwind: "$origin" },
  { $group: { _id: "$origin", kinds: { $sum: 1 } } },
  { $sort: { kinds: -1 } },
]);

// One-to-one relationship
// Create an id and store it in the variable owner
var owner = ObjectId();

// Create a document for the owner
db.owners.insertOne({
  _id: owner,
  name: "John Smith",
  contact: "09871236545",
});

// Change the "<owner_id" using the actual id in the previously created document
db.suppliers.insertOne({
  name: "ABC Fruits",
  contact: "09193219878",
  owner_id: owner,
});

// One-to-few relationship
db.suppliers.insertOne({
  name: "DEF Fruits",
  contact: "09179873212",
  addresses: [
    { street: "123 San Jose St.", city: "Manila" },
    { street: "367 Gil Puya", city: "Makati" },
  ],
});

// One-to-many relationship
var supplier = ObjectId();
var branch1 = ObjectId();
var branch2 = ObjectId();

db.suppliers.insertOne({
  _id: supplier,
  name: "GHI Fruits",
  contact: "09191234567",
  branches: [branch1],
});

db.branches.insertOne({
  _id: branch1,
  name: "BF Homes",
  address: "123 Arcardio Santos St.",
  city: "Paranaque",
  supplier_id: supplier,
});

db.branches.insertOne({
  _id: branch2,
  name: "BF Homes",
  address: "123 San Jose St.",
  city: "Manila",
  supplier_id: supplier,
});
